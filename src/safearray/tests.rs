use std::convert::{TryFrom, TryInto};

use ndarray::{arr2, arr3, ArrayD};

use crate::SafeArray;

fn init() {
    let _ = env_logger::builder().is_test(true).try_init();
}

fn matches<T: std::cmp::PartialEq>(a: Vec<T>, b: Vec<T>) -> bool {
    assert_eq!(a.len(), b.len());
    a.iter().zip(b.iter()).all(|(a, b)| a == b)
}

#[test]
fn test_boolean_vectors() {
    init();
    let src = vec![false, false, true, true, false, true];
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: Vec<bool> = sa.try_into().unwrap();
    assert!(matches(src, dest));
}

#[test]
fn test_i16_vectors() {
    init();
    let src = vec![0, 1, 2, 3, 4, 5, 6, -0, -1, -2, -3, -4, -5, -6];
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: Vec<i16> = sa.try_into().unwrap();
    assert!(matches(src, dest));
}

#[test]
fn test_i32_vectors() {
    init();
    let src = vec![0, 1, 2, 3, 4, 5, 6, -0, -1, -2, -3, -4, -5, -6];
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: Vec<i32> = sa.try_into().unwrap();
    assert!(matches(src, dest));
}

#[test]
fn test_i64_vectors() {
    init();
    let src = vec![0, 1, 2, 3, 4, 5, 6, -0, -1, -2, -3, -4, -5, -6];
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: Vec<i64> = sa.try_into().unwrap();
    assert!(matches(src, dest));
}

#[test]
fn test_f32_vectors() {
    init();
    let src = vec![
        0.0, 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, -0.0, -1.1, -2.2, -3.3, -4.4, -5.5, -6.6,
    ];
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: Vec<f32> = sa.try_into().unwrap();
    assert!(matches(src, dest));
}

#[test]
fn test_f64_vectors() {
    init();
    let src = vec![
        0.0, 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, -0.0, -1.1, -2.2, -3.3, -4.4, -5.5, -6.6,
    ];
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: Vec<f64> = sa.try_into().unwrap();
    assert!(matches(src, dest));
}

#[test]
fn test_u16_vectors() {
    init();
    let src = vec![0, 1, 2, 3, 4, 5, 6];
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: Vec<u16> = sa.try_into().unwrap();
    assert!(matches(src, dest));
}

#[test]
fn test_u32_vectors() {
    init();
    let src = vec![0, 1, 2, 3, 4, 5, 6];
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: Vec<u32> = sa.try_into().unwrap();
    assert!(matches(src, dest));
}

#[test]
fn test_u64_vectors() {
    init();
    let src = vec![0, 1, 2, 3, 4, 5, 6];
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: Vec<u64> = sa.try_into().unwrap();
    assert!(matches(src, dest));
}

#[test]
fn test_string_vectors() {
    init();
    let src = vec!["hello".to_string(), "world".to_string()];
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: Vec<String> = sa.try_into().unwrap();
    assert!(matches(src, dest));
}

#[test]
fn test_arrayd_boolean() {
    init();

    let src = arr2(&[[false, true], [true, false], [false, false], [true, true]]).into_dyn();
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: ArrayD<bool> = sa.try_into().unwrap();
    assert!(src.eq(&dest));
}

#[test]
fn test_arrayd_i16() {
    init();

    let src: ArrayD<i16> = arr2(&[[0, 1, 2, 3], [-4, -5, -6, -7]]).into_dyn();
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: ArrayD<i16> = sa.try_into().unwrap();
    assert!(src.eq(&dest));
}

#[test]
fn test_arrayd_i32() {
    init();

    let src: ArrayD<i32> = arr2(&[[0, 1, 2, 3], [-4, -5, -6, -7]]).into_dyn();
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: ArrayD<i32> = sa.try_into().unwrap();
    assert!(src.eq(&dest));
}

#[test]
fn test_arrayd_i64() {
    init();

    let src: ArrayD<i64> = arr2(&[[0, 1, 2, 3], [-4, -5, -6, -7]]).into_dyn();
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: ArrayD<i64> = sa.try_into().unwrap();
    assert!(src.eq(&dest));
}

#[test]
fn test_arrayd_f32() {
    init();

    let src: ArrayD<f32> = arr3(&[
        [[0.0, 1.1, 2.2], [3.3, 4.4, 5.5]],
        [[6.6, 7.7, 8.8], [9.9, 10.1, 11.11]],
    ])
    .into_dyn();
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: ArrayD<f32> = sa.try_into().unwrap();
    assert!(src.eq(&dest));
}

#[test]
fn test_arrayd_f64() {
    init();

    let src: ArrayD<f64> = arr3(&[
        [[0.0, 1.1, 2.2], [3.3, 4.4, 5.5]],
        [[6.6, 7.7, 8.8], [9.9, 10.1, 11.11]],
    ])
    .into_dyn();
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: ArrayD<f64> = sa.try_into().unwrap();
    assert!(src.eq(&dest));
}

#[test]
fn test_arrayd_u16() {
    init();

    let src: ArrayD<u16> = arr2(&[[0, 1, 2, 3], [4, 5, 6, 7]]).into_dyn();
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: ArrayD<u16> = sa.try_into().unwrap();
    assert!(src.eq(&dest));
}

#[test]
fn test_arrayd_u32() {
    init();

    let src: ArrayD<u32> = arr2(&[[0, 1, 2, 3], [4, 5, 6, 7]]).into_dyn();
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: ArrayD<u32> = sa.try_into().unwrap();
    assert!(src.eq(&dest));
}

#[test]
fn test_arrayd_u64() {
    init();

    let src: ArrayD<u64> = arr2(&[[0, 1, 2, 3], [4, 5, 6, 7]]).into_dyn();
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: ArrayD<u64> = sa.try_into().unwrap();
    assert!(src.eq(&dest));
}

#[test]
fn test_arrayd_string() {
    init();

    let src = arr2(&[
        ["hello", "world"],
        ["of", "multi"],
        ["dimensional", "arrays"],
    ])
    .into_dyn();
    let sa = SafeArray::try_from(&src).unwrap();
    let dest: ArrayD<String> = sa.try_into().unwrap();
    assert!(src.eq(&dest));
}
